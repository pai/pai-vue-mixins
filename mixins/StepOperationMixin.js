/*
* Il componente ereditante puo' sovrascrivere il metodo initStepOperation per eseguire logiche di inizializzazione
* prima dell'attivazione del primo step, dovrà poi chiamare goToStep(1) per iniziare l'operazione.
*
* All'attivazione di uno step (tramite goToStep(x)) il componente ereditante esegue la logica di
* entrata nello step in attivazione nella funzione tryActivateStep;
*
* Attivato lo step il componente ereditante esegue la logica dello stesso nella funzione stepActivated.
*
* A completamento avvenuto tale funzione chiamerà stepCompleted(indice, risultato, [prossimo_step]),
* nel caso in cui lo step non sia completato, si chiamerà la funzione goToStep(x).
*
*
* Il campo next del singolo step solitamente contiene la key dello step successivo
* puo tuttavia permettere una logica di abilitazione step esterni ad esempio
*
* - stepOperation con key 1
* - stepOperation con key 2
*
* uno step di 1 puo puntare allo step "fishing_pollutants" di 2 se ha "next": "2:fishing_pollutants",
* la step operation 2 al termine potrà tornare un risultato che può essere salvato nel result dello
* step chiamante.
* ...
*/

let gettext = window.gettext || function (s) { return s }

export default {
  props: {
    stepOperationString: {
      type: String,
      default: null
    }
  },
  data () {
    return {
      stepOperation: null,
      keyIndex: {},
      idIndex: {}
    }
  },
  created () {
    if (this.stepOperationString) {
      this.stepOperation = JSON.parse(this.stepOperationString)
    }
    for (let i = 0; i < this.stepOperation.steps.length; i++) {
      let step = this.stepOperation.steps[i]
      this.keyIndex[step.key] = i
      this.idIndex[i] = step.key
    }
  },
  mounted () {
    this.initStepOperation()
  },
  computed: {
    stepCount () {
      return this.stepOperation.steps.length
    },
    activeStep () {
      return this.stepOperation.activeStep
    }
  },
  methods: {
    /*
     * Private methods
     *
     */
    initStepOperation () {
      window.StepOp = this.stepOperation
      this.goToStep(this.getStepKeyByIndex(0))
    },
    getStepIndexByKey (key) {
      return this.keyIndex[key] || 0
    },
    getStepKeyByIndex (i) {
      return this.idIndex[i]
    },
    /*
     * Public methods
     *
     */
    clearStepOperation () {
      for (let i=0; i<this.stepOperation.steps.length; i++) {
        this.stepOperation.steps[i].error = this.stepOperation.steps[i].result = null
      }
    },
    clearNextSteps () {
      if (this.stepOperation.activeStep !== null) {
        let startI = this.getStepIndexByKey(this.stepOperation.activeStep)
        for (let i=startI; i<this.stepOperation.steps.length; i++) {
          this.stepOperation.steps[i].error = this.stepOperation.steps[i].result = null
        }
      }
    },
    getStepName (key) {
      let i = this.getStepIndexByKey(key)
      return gettext(this.stepOperation.steps[i].name)
    },
    getStepDescription (key) {
      let i = this.getStepIndexByKey(key)
      return gettext(this.stepOperation.steps[i].description)
    },
    getStepResult (key) {
      let i = this.getStepIndexByKey(key)
      return this.stepOperation.steps[i].result
    },
    setStepResult (key, val) {
      let i = this.getStepIndexByKey(key)
      this.stepOperation.steps[i].result = val
    },
    getStepError (key) {
      let i = this.getStepIndexByKey(key)
      return this.stepOperation.steps[i].error
    },
    setStepError (key, err) {
      let i = this.getStepIndexByKey(key)
      this.stepOperation.steps[i].error = err
    },
    stepCompleted (key, result, next) {
      let i = this.getStepIndexByKey(key)
      let step = this.stepOperation.steps[i]
      // console.log('Step completed', key, i, step, 'with result', result, next)

      this.setStepResult(key, result)
      this.setStepError(key, null)
      if (next) {
        this.activateStep(next)
      } else {
        if (step.next) {
          // console.log('StepOperationMixin got next step index', step.next)
          this.activateStep(step.next)
        } else {
          // console.log('StepOperationMixin error, no next step for', i, step)
        }
      }
    },
    stepFailed (key, error, next) {
      // let i = this.getStepIndexByKey(key)
      // let step = this.stepOperation.steps[i]
      // console.log('Step failed', key, this.keyIndex, i, step, 'with error', error)
      this.setStepError(key, error)
      if (next) {
        this.activateStep(next)
      }
    },
    /*
     * step around methods
     *
     */
    goToStep (key) {
      this.stepOperation.activeStep = key
    },
    activateStep (key) {
      this.tryActivateStep(key)
    },
    /*
     * Overridable methods
     *
    */
    tryActivateStep (key) {
      // should call goToStep
      // console.log('trying step activation', step)
      this.goToStep(key)
    },
    stepActivated () { // (key)
      // console.log('step activated', step)
    }
  },
  watch: {
    activeStep (n) {
      // console.log('stepOperation.activeStep changed from', oldStep, 'to', newStep)
      this.stepActivated(n)
      this.$emit('stepActivated', n)
    }
  }
}
