export default {
  props: {
    loaded: {
      type: Boolean,
      default: true
    },
    loadingText: {
      type: String,
      default: '..'
    }
  },
  data () {
    return {
      loadingTasks: 0
    }
  },
  computed: {
    loading () {
      return (!this.loaded || this.loadingTasks > 0)
    }
  },
  methods: {
    startLoading () {
      this.loadingTasks += 1
      this.$emit('startLoading')
    },
    stopLoading () {
      this.loadingTasks -= 1
      this.$emit('stopLoading')
    }
  }
}
