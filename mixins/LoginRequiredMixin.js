import GetBrowserTypeMixin from './GetBrowserTypeMixin'

export default {
  mixins: [
    GetBrowserTypeMixin
  ],
  props: {
    autoLoginRedirect: {
      type: Boolean,
      default: true
    },
    loginUrl: {
      type: String,
      default: '/api/v1/auth/login/'
    },
    alreadyLoggedIn: {
      type: Boolean,
      default: false
    }
  },
  data () {
    return {
      loggedIn: false,
      logginIn: false
    }
  },
  methods: {
    performLogin (ev) {
      this.logginIn = true
      let loginUrl = this.loginUrl + '?next=' + window.location.href

      if (this.autoLoginRedirect || this.browserType.ie || this.browserType.edge) {
        window.location.href = loginUrl
      } else {
        this.$emit('askLogin', ev)
      }
      this.logginIn = false
    }
  }
}
