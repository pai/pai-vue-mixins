export default {
  props: {
    entities: {
      type: Array,
      default () {
        return []
      }
    }
  }
}
