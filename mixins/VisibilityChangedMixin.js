// v-observe-visibility="visibilityChanged"

export default {
  data () {
    return {
      visible: false
    }
  },
  mounted () {
    let _isSht = /MSIE 10/i.test(navigator.userAgent) || /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /Edge\/\d./i.test(navigator.userAgent)
    if (_isSht) {
      this.isVisible()
    }
  },
  methods: {
    visibilityChanged (ev) {
      if (!this.auto && !this.visible && ev) {
        // console.log('Vis changed for', this, ev)
        this.visible = true
        this.isVisible()
      }
    },
    isVisible () { // (ev)
      // console.log('is visible', this, ev)
    }
  }
}
