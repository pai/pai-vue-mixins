export default {
  props: {
    entity: {
      type: Object,
      default: null
    }
  }
}
