let gettext = window.gettext || function (s) { return s }
let genericError = gettext('Something happened in setting up the request that triggered an Error. Please contact support.')

export default {
  methods: {
    parseError: function (error) {
      if (error.response) {
        // let status = error.response.status
        let response = error.response.data
        let message = ''

        if (typeof response === 'object') {
          for (let property in response) {
            if (response.hasOwnProperty(property)) {
              message += ' ' + response[property] + ' (' + property + ') '
            }
            return message
          }
        } else if (typeof response === 'string') {
          return response
        } else {
          return genericError
        }
      } else if (error.request) {
        return gettext('Server is unreacheable, please check your connection or try later.')
      } else {
        return genericError
      }
    }
  }
}
