export default {
  data () {
    return {
      xhrPatchMaxRetries: 0,
      xhrPatchRetries: 0,
      xhrPatchParams: {},
      xhrPatchOptions: {},
      xhrPatchUrl: null,
      xhrPatchPermittedErrors: '',
      xhrPatchAuthErrors: '401,403',
      xhrPatchRetryTimeout: 199,
      xhrPatchRetryOnPermittedError: false,
      xhrPatchRetryIfNotAuthorized: false
    }
  },
  methods: {
    xhrPatchError (error) {
      let hasResponse = error.response
      let permittedError = hasResponse && this.xhrPatchPermittedErrors.indexOf(error.response.status) > -1
      let notAuthorized = hasResponse && this.xhrPatchAuthErrors.indexOf(error.response.status) > -1
      let hasRetries = this.xhrPatchRetries < this.xhrPatchMaxRetries

      let retries = !hasResponse || (permittedError && this.xhrPatchRetryOnPermittedError) || (notAuthorized && this.xhrPatchRetryIfNotAuthorized)

      if (retries && hasRetries) {
        this.xhrPatchRetries += 1

        setTimeout(() => {
          this.performXhrPatch()
        }, this.xhrPatchRetryTimeout)
      } else {
        let msg = 'Error performing PATCH request'
        if (!hasResponse) {
          msg += ' (No response)'
        }
        if (notAuthorized) {
          msg += ' (Not authorized)'
        }
        if (hasResponse && !permittedError) {
          msg += ' (Error ' + error.response.status + ')'
        }
        if (!hasRetries) {
          msg += ' (No reties left)'
        }
        this.$emit('xhrPatchError', error)
        if (!hasResponse || !permittedError) throw msg
      }
    },
    performXhrPatch () {
      if (this.xhrPatchRetries === 0) {
        this.$emit('performingXhrPatch', this.name)
      }

      this.$http.patch(
        this.xhrPatchUrl,
        this.xhrPatchParams,
        this.xhrPatchOptions
      ).then((response) => {
        this.$emit('xhrPatchResponse', response)
      }).catch((error) => {
        this.xhrPatchError(error)
      })
    }
  }
}
