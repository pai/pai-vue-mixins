export default {
  props: {
    entitiesPerPage: {
      type: String,
      default: 10
    }
  },
  data () {
    return {
      pagination: {
        currentPage: 1,
        entitiesPerPageD: this.entitiesPerPage
      }
    }
  },
  methods : {
    gotoPage (pageNumber) {
      if (0 < pageNumber && pageNumber <= this.totalPages) {
        this.pagination.currentPage = pageNumber
      }
    }
  },
  computed: {
    paginatedEntities () {
      // Must be set by importing module
    },
    offset () { return (this.pagination.currentPage - 1) * this.pagination.entitiesPerPageD },
    totalPages () { return Math.ceil(this.paginatedEntities.length / this.pagination.entitiesPerPageD) },
    prevPage () { return this.pagination.currentPage - 1 },
    nextPage () { return this.pagination.currentPage + 1 },
    pages () {
      let a = []

      for (let i = 0; i < this.totalPages; i++) {
          a.push(i + 1)
      }

      return a
    }
  },
  watch: {
    entitiesPerPage (n) {
      this.entitiesPerPageD = n
    }
  }
}