export default {
  props: {
    entityPermissions: {
      type: Array,
      default () {
        return [
          {
            'slug': 'public',
            'name': 'Public'
          },
          {
            'slug': 'private',
            'name': 'Private'
          },
          {
            'slug': 'shared',
            'name': 'Shared'
          }
        ]
      }
    }
  },
  data () {
    return {
      selectedPermissionSlug: 'public',
      selectedPermission: this.entityPermissions[0]
    }
  },
  methods: {
    findPermissionBySlug (s) {
      for (let i=0; i<this.entityPermissions.length; i++) {
        let x = this.entityPermissions[i]
        if (x.slug === s) {
          return x
        }
      }
    },
    selectPermission (p) {
      this.$emit('selectedPermission', p)
    },
    selectPermissionBySlug (s) {
      this.selectedPermission = this.findPermissionBySlug(s)
      this.$emit('selectedPermission', this.selectedPermission)
    }
  }
}
