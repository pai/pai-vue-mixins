export default {
  data () {
    return {
      xhrPostMaxRetries: 0,
      xhrPostRetries: 0,
      xhrPostParams: {},
      xhrPostOptions: {},
      xhrPostUrl: null,
      xhrPostPermittedErrors: '',
      xhrPostAuthErrors: '401,403',
      xhrPostRetryTimeout: 199,
      xhrPostRetryOnPermittedError: false,
      xhrPostRetryIfNotAuthorized: false
    }
  },
  methods: {
    xhrPostError (error) {
      let hasResponse = error.response
      let permittedError = hasResponse && this.xhrPostPermittedErrors.indexOf(error.response.status) > -1
      let notAuthorized = hasResponse && this.xhrPostAuthErrors.indexOf(error.response.status) > -1
      let hasRetries = this.xhrPostRetries < this.xhrPostMaxRetries

      let retries = !hasResponse || (permittedError && this.xhrPostRetryOnPermittedError) || (notAuthorized && this.xhrPostRetryIfNotAuthorized)

      if (retries && hasRetries) {
        this.xhrPostRetries += 1

        setTimeout(() => {
          this.performXhrPost()
        }, this.xhrPostRetryTimeout)
      } else {
        let msg = 'Error performing POST request'
        if (!hasResponse) {
          msg += ' (No response)'
        }
        if (notAuthorized) {
          msg += ' (Not authorized)'
        }
        if (hasResponse && !permittedError) {
          msg += ' (Error ' + error.response.status + ')'
        }
        if (!hasRetries) {
          msg += ' (No reties left)'
        }
        this.$emit('xhrPostError', error)
        if (!hasResponse || !permittedError) throw msg
      }
    },
    performXhrPost () {
      if (this.xhrPostRetries === 0) {
        this.$emit('performingXhrPost', this.name)
      }

      this.$http.post(
        this.xhrPostUrl,
        this.xhrPostParams,
        this.xhrPostOptions
      ).then((response) => {
        this.$emit('xhrPostResponse', response)
      }).catch((error) => {
        this.xhrPostError(error)
      })
    }
  }
}
