/*
Check if there is a next url param
*/

export default {
  data () {
    return {
      nextUrl: null
    }
  },
  mounted () {
    this.nextUrl = this.$route.query.next
    // console.log('nextUrl', this.nextUrl)
  },
  computed: {
    nextUrlParam () {
      if (this.nextUrl) {
        return '?next=' + this.nextUrl
      } else {
        return ''
      }
    }
  },
  methods: {
    getBasePath (link) {
      // console.log('get base path', link)
      let pathArray = link.trim().split('/')
      let url = '/'
      if (pathArray.length) {
          let protocol = pathArray[0]
          let host = pathArray[2]
          if (protocol && protocol.trim() != '' && host && host.trim() != '') {
            url = protocol + '//' + host
          } else if (host && host.trim() !== '') {
            url = host
          }
      }
      return url
    },
    followLink (link) {
      // console.log('redirecting to', link)
      window.location.href = link
    },
    askFollowLink (ev) {
      let link = ev.target.href
      if (this.nextUrl) {
        link += '?next=' + this.nextUrl
      }
      this.followLink(link)
    },
    askFollowBasePathLink (ev) {
      let link = ev.target.href
      if (this.nextUrl) {
        let url = this.getBasePath(this.nextUrl)
        link += '?next=' + url
      }
      this.followLink(link)
    },
    followNextUrl () {
      if (this.nextUrl) {
        this.followLink(this.nextUrl)
      }
    }
  }
}
