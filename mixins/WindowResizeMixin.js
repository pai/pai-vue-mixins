// Mixin providing reisizable element
// Importing element must define ref="container" to its main element

export default {
  data () {
    return {
      windowWidth: 320,
      windowHeight: 240
    }
  },
  mounted () {
    window.addEventListener('resize', (ev) => {
      this.resize(ev)
    })

    this.$on('windowResized', (ev) => {
      // console.log('resized', ev)
      this.windowWidth = ev.width
      this.windowHeight = ev.height
    })
  },
  methods: {
    resize () { // (ev)
      let w = window
      let d = document
      let e = d.documentElement
      let g = d.getElementsByTagName('body')[0]
      let x = w.innerWidth || e.clientWidth || g.clientWidth
      let y = w.innerHeight || e.clientHeight || g.clientHeight
      this.$emit('windowResized', {width: x, height: y})
    }
  }
}
