export default {
  data () {
    return {
      xhrGetMaxRetries: 0,
      xhrGetRetries: 0,
      xhrGetParams: {},
      xhrGetOptions: {},
      xhrGetUrl: null,
      xhrGetPermittedErrors: '',
      xhrGetAuthErrors: '401,403',
      xhrGetRetryTimeout: 199,
      xhrGetRetryOnPermittedError: false,
      xhrGetRetryIfNotAuthorized: false
    }
  },
  methods: {
    xhrGetError (error) {
      let hasResponse = error.response
      let permittedError = hasResponse && this.xhrGetPermittedErrors.indexOf(error.response.status) > -1
      let notAuthorized = hasResponse && this.xhrGetAuthErrors.indexOf(error.response.status) > -1
      let hasRetries = this.xhrGetRetries < this.xhrGetMaxRetries

      let retries = !hasResponse || (permittedError && this.xhrGetRetryOnPermittedError) || (notAuthorized && this.xhrGetRetryIfNotAuthorized)

      if (retries && hasRetries) {
        this.xhrGetRetries += 1

        setTimeout(() => {
          this.performXhrGet()
        }, this.xhrGetRetryTimeout)
      } else {
        let msg = 'Error performing GET request'
        if (!hasResponse) {
          msg += ' (No response)'
        }
        if (notAuthorized) {
          msg += ' (Not authorized)'
        }
        if (hasResponse && !permittedError) {
          msg += ' (Error ' + error.response.status + ')'
        }
        if (!hasRetries) {
          msg += ' (No reties left)'
        }
        this.$emit('xhrGetError', error)
        if (!hasResponse || !permittedError) throw msg
      }
    },
    performXhrGet () {
      if (this.xhrGetRetries === 0) {
        this.$emit('performingXhrGet', this.name)
      }

      this.$http.get(
        this.xhrGetUrl,
        this.xhrGetParams,
        this.xhrGetOptions
      ).then((response) => {
        this.$emit('xhrGetResponse', response)
      }).catch((error) => {
        this.xhrGetError(error)
      })
    }
  }
}
