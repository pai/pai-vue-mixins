export default {
  data () {
    return {
      xhrPutMaxRetries: 0,
      xhrPutRetries: 0,
      xhrPutParams: {},
      xhrPutOptions: {},
      xhrPutUrl: null,
      xhrPutPermittedErrors: '',
      xhrPutAuthErrors: '401,403',
      xhrPutRetryTimeout: 199,
      xhrPutRetryOnPermittedError: false,
      xhrPutRetryIfNotAuthorized: false
    }
  },
  methods: {
    xhrPutError (error) {
      let hasResponse = error.response
      let permittedError = hasResponse && this.xhrPutPermittedErrors.indexOf(error.response.status) > -1
      let notAuthorized = hasResponse && this.xhrPutAuthErrors.indexOf(error.response.status) > -1
      let hasRetries = this.xhrPutRetries < this.xhrPutMaxRetries

      let retries = !hasResponse || (permittedError && this.xhrPutRetryOnPermittedError) || (notAuthorized && this.xhrPutRetryIfNotAuthorized)

      if (retries && hasRetries) {
        this.xhrPutRetries += 1

        setTimeout(() => {
          this.performXhrPut()
        }, this.xhrPutRetryTimeout)
      } else {
        let msg = 'Error performing PUT request'
        if (!hasResponse) {
          msg += ' (No response)'
        }
        if (notAuthorized) {
          msg += ' (Not authorized)'
        }
        if (hasResponse && !permittedError) {
          msg += ' (Error ' + error.response.status + ')'
        }
        if (!hasRetries) {
          msg += ' (No reties left)'
        }
        this.$emit('xhrPutError', error)
        if (!hasResponse || !permittedError) throw msg
      }
    },
    performXhrPut () {
      if (this.xhrPutRetries === 0) {
        this.$emit('performingXhrPut', this.name)
      }

      this.$http.put(
        this.xhrPutUrl,
        this.xhrPutParams,
        this.xhrPutOptions
      ).then((response) => {
        this.$emit('xhrPutResponse', response)
      }).catch((error) => {
        this.xhrPutError(error)
      })
    }
  }
}
