// Mixin providing reisizable element
// Importing element must define ref="container" to its main element

export default {
  props: {
    ratio: {
      type: Number,
      default: 1
    }
  },
  data () {
    return {
      width: 0,
      height: 0
    }
  },
  mounted () {
    // 100:120/h:21
    window.addEventListener('resize', () => {
    // console.log('resize', ev)
      this.resize()
    })

    this.$nextTick(() => {
      this.resize()
    })
  },
  methods: {
    resize () {
      let w = this.$refs.container.clientWidth
      let h = w * this.ratio
      this.width = w
      this.height = h
    }
  }
}
